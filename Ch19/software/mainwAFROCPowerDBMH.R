#mainwAFROCPowerDBMH.R
rm(list = ls()) 
library(ggplot2)
library(RJafroc)

# included datasets
fileNames <-  c("TONY", "VD", "FR", 
                "FED", "JT", "MAG", 
                "OPT", "PEN", "NICO",
                "RUS", "DOB1", "DOB2", 
                "DOB3", "FZR")

f <- 4
fileName <- fileNames[f]
theData <- get(sprintf("dataset%02d", f))
# RSM ROC fitting needs to know lesionDistribution
nLesDistr <- UtilLesionDistribution(theData)


retFileName <- paste0("allResults", fileName) 
sysAnalFileName <- system.file(
  "ANALYZED/RSM6", 
  retFileName, 
  package = "RJafroc")
load(sysAnalFileName)
I <- allResults[[1]]$I
J <- allResults[[1]]$J

mu <- array(dim = c(I, J))
nuP <- array(dim = c(I, J))
lambdaP <- array(dim = c(I, J))
lambda <- array(dim = c(I, J))
nu <- array(dim = c(I, J))
AllResIndx <- 0
for (i in 1:I){
  for (j in 1:J){
    AllResIndx <- AllResIndx + 1
    mu[i,j] <- allResults[[AllResIndx]]$retRsm$mu
    lambdaP[i,j] <- allResults[[AllResIndx]]$retRsm$lambdaP
    nuP[i,j] <- allResults[[AllResIndx]]$retRsm$nuP
    x <- UtilPhysical2IntrinsicRSM(
      mu[i,j], lambdaP[i,j], nuP[i,j])
    lambda[i,j] <- x$lambda
    nu[i,j] <- x$nu
  }
}

# FED data has 5 modalities; we choose to analyze the first two
i1 <- 1;i2 <- 2 
cat("NH i1 = ", i1, "NH i2 = ", i2, "\n")
selectJ <- c(1, 2, 3, 4)
frocData <- DfExtractDataset(
  theData, trts = c(i1, i2), rdrs = selectJ)
J <- length(frocData$readerID)
K <- dim(frocData$NL)[3]

mu1 <- mu[1,]
mu2 <- mu[2,]
nu1 <- nu[1,]
nu2 <- nu[2,]
lambda1 <- lambda[1,]
lambda2 <- lambda[2,]

mu <- rbind(mu1, mu2)
lambda <- rbind(lambda1, lambda2)
nu <- rbind(nu1, nu2)
# instead of average, use median to get representative value over whole dataset
muMed <- median(mu) 
nuMed <- median(nu) # do:
lambdaMed <- median(lambda) # do:

# construct lesion weights, assuming equally weighted lesions
lesionWeights <- matrix(-Inf, nrow = nrow(nLesDistr), ncol = nrow(nLesDistr))
for (l in 1:nrow(nLesDistr)){
  nLes <- nLesDistr[l, 1]
  lesionWeights[l, 1:nLes] <- 1/nLes
}
# calculate NH values for ROC-AUC and wAFROC-AUC
aucRocNH <- PlotRsmOperatingCharacteristics(
  muMed, lambdaMed, nuMed, 
  lesionDistribution = nLesDistr, lesionWeights = lesionWeights)$aucROC
aucAfrocNH <- PlotRsmOperatingCharacteristics(
  muMed, lambdaMed, nuMed, 
  lesionDistribution = nLesDistr, lesionWeights = lesionWeights)$aucwAFROC

# following code calculates ROC-ES and wAFROC-ES
deltaMu <- seq(0.01, 0.2, 0.01) # values of deltaMu to scan below
esRoc <- array(dim = length(deltaMu))
eswAfroc <- array(dim = length(deltaMu))
for (i in 1:length(deltaMu)) {
  esRoc[i] <- PlotRsmOperatingCharacteristics(
    muMed + deltaMu[i], 
    lambdaMed, nuMed, 
    lesionDistribution = nLesDistr, 
    lesionWeights = lesionWeights, 
    type = "ROC")$aucROC - aucRocNH
  eswAfroc[i] <- PlotRsmOperatingCharacteristics(
    muMed+ deltaMu[i], 
    lambdaMed, 
    nuMed, 
    lesionDistribution = nLesDistr, 
    lesionWeights = lesionWeights, 
    type = "wAFROC")$aucwAFROC - aucAfrocNH
  cat("ES ROC, wAFROC = ", esRoc[i], eswAfroc[i],"\n")
}
cat("\n")

a<-lm(eswAfroc~ 0+esRoc) # fit values to straight line thru origin
effectSizeROC <- seq(0.01, 0.1, 0.001)
effectSizewAFROC <- effectSizeROC*a$coefficients[1]# r2 = summary(a)$r.squared

JTest <- 5;KTest <- 100
varCompROC <- StSignificanceTesting(
  frocData, 
  FOM = "HrAuc", 
  method = "DBMH", 
  option = "RRRC")$varComp
varCompwAFROC <- StSignificanceTesting(
  frocData, 
  FOM = "wAFROC", 
  method = "DBMH", 
  option = "RRRC")$varComp

cat("JTest = ", JTest, "KTest = ", KTest, "\n")
powerROC <- array(dim = length(effectSizeROC))
powerwAFROC <- array(dim = length(effectSizeROC))
for (i in 1:length(effectSizeROC)) {
  varYTR <- varCompROC$varComp[3]
  varYTC <- varCompROC$varComp[4]
  varYEps <- varCompROC$varComp[6]
  powerROC[i] <- SsPowerGivenJK(
    JTest, 
    KTest, 
    alpha = 0.05, 
    effectSize = effectSizeROC[i], 
    option = "RRRC",
    method = "DBMH", 
    varYTR = varYTR, 
    varYTC = varYTC, 
    varYEps = varYEps)$powerRRRC
  
  varYTR <- varCompwAFROC$varComp[3]
  varYTC <- varCompwAFROC$varComp[4]
  varYEps <- varCompwAFROC$varComp[6]
  powerwAFROC[i] <- SsPowerGivenJK(
    JTest, KTest, 
    alpha = 0.05, 
    effectSize = effectSizewAFROC[i], 
    option = "RRRC",
    method = "DBMH", 
    varYTR = varYTR, 
    varYTC = varYTC, 
    varYEps = varYEps)$powerRRRC
  
  cat("ROC effect-size = ,", effectSizeROC[i], 
      "wAFROC effect-size = ,", effectSizewAFROC[i], 
      "Power ROC, wAFROC:", powerROC[i], ",", powerwAFROC[i], "\n")
}

df <- data.frame(esRoc = esRoc, eswAfroc = eswAfroc)
p <- ggplot(data = df, aes(x = esRoc, y = eswAfroc)) +
  geom_smooth(method = "lm", se = FALSE, color = "black", formula = y ~ x) +
  geom_point()
print(p)

df <- data.frame(powerROC = powerROC, powerwAFROC = powerwAFROC)
p <- ggplot(mapping = aes(x = powerROC, y = powerwAFROC)) +
  geom_line(data = df, size = 0.5)
print(p)
