

 07/29/2017

                             OR-DBM MRMC 2.5  Build 4

              MULTIREADER-MULTICASE ROC ANALYSIS OF VARIANCE
                          PROPROC AREA ANALYSIS
 
 
 |====================================================================|
 |*****                         Credits                          *****|
 |====================================================================|
 |                                                                    |
 | ANOVA Computations & Display:                                      |
 | -----------------------------                                      |
 | Kevin M. Schartz, Stephen L. Hillis, Lorenzo L. Pesce, &           |
 | Kevin S. Berbaum                                                   |
 |                                                                    |
 |====================================================================|
 
 
 
 |====================================================================|
 |***************************     NOTE     ***************************|
 |====================================================================|
 | The user agreement for this software stipulates that any           |
 | publications based on research data analyzed using this software   |
 | must cite references 1-5 given below.                              |
 |                                                                    |
 | Example of citing the software:                                    |
 |                                                                    |
 |      Reader performance analysis was performed using the software  |
 | package OR-DBM MRMC 2.4, written by Kevin M.Schartz, Stephen L.    |
 | Hillis, Lorenzo L. Pesce, and Kevin S. Berbaum, and freely         |
 | available at http://perception.radiology.uiowa.edu. This program   |
 | is based on the methods initially proposed by Berbaum, Dorfman,    |
 | and Metz [1] and Obuchowski and Rockette [2] and later unified and |
 | improved by Hillis and colleagues [3-5].                           |
 |====================================================================|
 
 Data file: \\vmware-host\Shared Folders\VmWareShared\VD\VD.lrc                                                                                                                                                                                                             
 
 2 treatments, 5 readers, 114 cases (69 normal, 45 abnormal)
 
 Curve fitting methodology is PROPROC
 Dependent variable is AUC
 
 Study Design:  Factorial
 Covariance Estimation Method:  Jackknifing
 
 ===========================================================================
 *****                            Estimates                            *****
 ===========================================================================
 
TREATMENT x READER AUC ESTIMATES

                  TREATMENT
           -----------------------
 READER         1            2
 ------    ----------   ----------   
     1     0.93404036   0.93161194
     2     0.89107141   0.92599258
     3     0.90783214   0.93043176
     4     0.97745948   1.00000000
     5     0.84055976   0.94268741
 

 TREATMENT AUC MEANS (averaged across readers)
 ---------------------------------------------
       1      0.91019263
       2      0.94614474
 
 

 TREATMENT AUC MEAN DIFFERENCES
 ------------------------------
     1 - 2    -0.03595211
 
 
 
 ===========================================================================
 *****            ANOVA Tables (OR analysis of reader AUCs)            *****
 ===========================================================================
 
 TREATMENT X READER ANOVA of AUCs
 (Used for global test of equal treatment AUCs and for treatment differences
  confidence intervals in parts (a) and (b) of the analyses)
 
Source            SS               DF             MS        
------   --------------------    ------   ------------------
     T             0.00323139         1           0.00323139
     R             0.01098428         4           0.00274607
   T*R             0.00310575         4           0.00077644
 
 
 
 READER ANOVAs of AUCs for each treatment
 (Used for single treatment confidence intervals in part (c) of the analyses)
 

                        Mean Squares
 Source     df   Treatment 1   Treatment 2
 ------    ---   -----------   -----------
      R      4    0.00257837    0.00094414
 
 
 ===========================================================================
 *****        Variance component and error-covariance estimates        *****
 ===========================================================================
 
 Obuchowski-Rockette variance component and covariance estimates
 (for sample size estimation for future studies)
 Note: These are ANOVA estimates which can be negative
 
     OR Component             Estimate         Correlation  
 -----------------------  ----------------  ----------------
 Var(R)                         0.00100262
 Var(T*R)                      -0.00035204
 COV1                           0.00003078        0.02218308
 COV2                           0.00027710        0.19966989
 COV3                           0.00004859        0.03500989
 Var(Error)                     0.00138777
 
 
 Corresponding DBM variance component and covariance estimates
 
     DBM Component            Estimate    
 -----------------------  ----------------
 Var(R)                         0.00100262
 Var(C)                         0.00553876
 Var(T*R)                      -0.00035204
 Var(T*C)                       0.02605014
 Var(R*C)                      -0.00202927
 Var(T*R*C) + Var(Error)        0.12864601
 
 
 ===========================================================================
 *****    Analysis 1 (OR Analysis): Random Readers and Random Cases    *****
 ===========================================================================
 (Results apply to the population of readers and cases)


    a) Test for H0: Treatments have the same AUC
 
 Source        DF      Mean Square    F value  Pr > F 
 ----------  ------  ---------------  -------  -------
 Treatment        1       0.00323139     1.68   0.2065
 Error term   24.43       0.00191899
 Error term = MS(T*R) + r*max[Cov2 - Cov3,0]
 
 Conclusion: The treatment AUCs are not significantly different [F(1,24) = 1.68, p = .2065].
 
 Df(error term) = [MS(T*R) + r*max(Cov2 - Cov3,0)]**2/{MS(T*R)**2/[(t-1)(r-1)]}
 Note: "Error term" is the denominator of the F statistic and is a linear
 combination of mean squares, as defined above.  The value of this linear 
 combination is given under the "Mean Square" column
 Note: Df(error term) is called "ddf_H" in Hillis (2007).
 

    b) 95% confidence intervals and hypothesis tests (H0: difference = 0)
       for treatment AUC differences
 
 Treatment
 Comparison  Difference   StdErr      DF      t     Pr >|t|          95% CI       
 ----------  ----------  --------  -------  ------  -------  ---------------------
   1 - 2       -0.03595   0.02771    24.43   -1.30   0.2065  (-0.09308 ,  0.02118)
 
 StdErr = sqrt{(2/r)*[MS(T*R) + r*max(Cov2 - Cov3,0)]}
 Df same as df(error term) from (a)
 95% CI: Difference +- t(.025;df) * StdErr
 

    c) Single-treatment 95% confidence intervals
       (Each analysis is based only on data for the specified treatment, i.e., 
       on the treatment-specific reader ANOVA of AUCs and Cov2 estimates.)
 
  Treatment      AUC      Std Err       DF     95% Confidence Interval      Cov2   
 ----------  ----------  ----------  -------  -------------------------  ----------
          1  0.91019263  0.03065034    13.28  (0.84411602 , 0.97626924)  0.00042377
          2  0.94614474  0.01786755    11.43  (0.90699979 , 0.98528969)  0.00013042
 
 StdErr = sqrt{1/r * [MS(R) + r*max(Cov2,0)]}
 Df = [MS(R)+ max(r*cov2,0)]**2/[(MS(R)**2/(r-1)]
 Note: Df is called "ddf_H_single" in Hillis (2007)
 95% CI: AUC +- t(.025;df) * StdErr
 
 
 ===========================================================================
 *****    Analysis 2 (OR Analysis): Fixed Readers and Random Cases     *****
 ===========================================================================
 (Results apply to the population of cases but only for the readers used in
 this study. Chi-square or Z tests are used; these are appropriate for 
 moderate or large case sample sizes.)
 
    a) Chi-square test for H0: Treatments have the same AUC
    Note: The chi-square statistic is denoted by X2 or by X2(df), where df is its 
    corresponding degrees of freedom.
 
 
     X2 value       DF    Pr > X2
 ---------------  ------  -------
         1.42288       1   0.2329
 
 Conclusion: The treatment AUCs are not significantly different [X2(1) =  1.42, p = .2329].
 
 X2 = (t-1)*MS(T)/[Var(error) - Cov1 + (r-1)*max(Cov2 - Cov3,0)]


    b) 95% confidence intervals and hypothesis tests (H0: difference = 0)
       for treatment AUC differences
 
 Treatment
 Comparison  Difference   StdErr     z     Pr >|z|          95% CI       
 ----------  ----------  --------  ------  -------  ---------------------
   1 - 2       -0.03595   0.03014   -1.19   0.2329  (-0.09503 ,  0.02312)
 
 StdErr = sqrt{2/r * [(Var(error) - Cov1 + (r-1)*max(Cov2 - Cov3,0)]}
 95% CI: difference +- z(.025) * StdErr
 

    c) Single treatment AUC 95% confidence intervals
       (Each analysis is based only on data for the specified treatment, i.e., on
        the specific reader ANOVA of AUCs and error-variance and Cov2 estimates.)
 
  Treatment      AUC      Std Error   95% Confidence Interval 
 ----------  ----------  ----------  -------------------------
          1  0.91019263  0.02799706  (0.85531940 , 0.96506587)
          2  0.94614474  0.01465008  (0.91743110 , 0.97485838)
 
  Treatment  Var(Error)     Cov2   
 ----------  ----------  ----------
          1  0.00222410  0.00042377
          2  0.00055144  0.00013042
 
 StdErr = sqrt{1/r * [Var(error) + (r-1)*max(Cov2,0)]}
 95% CI: AUC +- z(.025) * StdErr


    d) Single-reader 95% confidence intervals and tests (H0: difference = 0) for 
    treatment AUC differences.
       (Each analysis is based only on data for the specified reader, i.e, on the 
        reader-specific AUC, error-variance and Cov1 estimates.)
 
         Treatment
 Reader  Comparison  Difference  StdErr      z     Pr >|z|          95% CI       
 ------  ----------  ----------  --------  ------  -------  ---------------------
      1    1 - 2        0.00243   0.04065    0.06   0.9524  (-0.07725 ,  0.08211)
      2    1 - 2       -0.03492   0.05404   -0.65   0.5181  (-0.14084 ,  0.07100)
      3    1 - 2       -0.02260   0.07665   -0.29   0.7681  (-0.17283 ,  0.12763)
      4    1 - 2       -0.02254   0.01216   -1.85   0.0638  (-0.04637 ,  0.00129)
      5    1 - 2       -0.10213   0.05453   -1.87   0.0611  (-0.20901 ,  0.00475)
 
 Reader  Var(Error)     Cov1   
 ------  ----------  ----------
      1  0.00070681  -.00011955
      2  0.00126446  -.00019573
      3  0.00349022  0.00055255
      4  0.00007393  0.00000000
      5  0.00140343  -.00008335
 
 StdErr = sqrt[2*(Var(error) - Cov1)]
 95% CI: Difference +- z(.025) * StdErr
 
 
 ===========================================================================
 *****    Analysis 3 (OR Analysis): Random Readers and Fixed Cases     *****
 ===========================================================================
 (Results apply to the population of readers but only for the cases used in
 this study)

     These results result from using the OR model, but treating reader as a random 
 factor and treatment and case as fixed factors.  Because case is treated as a fixed
 factor, it follows that Cov1 = Cov2 = Cov3 = 0; i.e., there is no correlation
 between reader-performance measures (e.g, AUCs) due to reading the same
 cases.  Thus the OR model reduces to a conventional treatment x reader ANOVA
 for the reader-performance outcomes, where reader is a random factor and
 treatment is a fixed factor.  This is the same as a repeated measures ANOVA
 where treatment is the repeated measures factor, i.e., readers provide an
 outcome (e.g., AUC) under each treatment.
     Note that the DBM and OR papers do not discuss this approach, but rather 
 it is included here for completeness.

    a) Test for H0: Treatments have the same AUC
 
 Source        DF    Mean Square      F value  Pr > F 
 ----------  ------  ---------------  -------  -------
 Treatment        1       0.00323139     4.16   0.1109
 T*R              4       0.00077644
 
 Conclusion: The treatment AUCs are not significantly different [F(1,4) = 4.16, p = .1109].
 Note: If there are only 2 treatments, this is equivalent to a paired t-test applied
 to the AUCs


    b) 95% confidence intervals and hypothesis tests (H0: difference = 0)
       for treatment AUC differences
 
 Treatment
 Comparison  Difference   StdErr      DF      t     Pr >|t|          95% CI       
 ----------  ----------  --------  -------  ------  -------  ---------------------
   1 - 2       -0.03595   0.01762        4   -2.04   0.1109  (-0.08488 ,  0.01298)
 
 StdErr = sqrt[2/r * MS(T*R)]
 DF = df[MS(T*R)] = (t-1)(r-1)
 95% CI: Difference +- t(.025;df) * StdErr
 Note: If there are only 2 treatments, this is equivalent to a paired t-test applied
 to the AUCs
 

    c) Single treatment AUC 95% confidence intervals
       (Each analysis is based only on data for the specified treatment, 
       i.e. on the treatment-specfic reader ANOVA of AUCs
 
  Treatment      AUC        MS(R)     Std Error     DF     95% Confidence Interval 
 ----------  ----------  ----------  ----------  -------  -------------------------
          1  0.91019263  0.00257837  0.02270845        4  (0.84714387 , 0.97324139)
          2  0.94614474  0.00094414  0.01374145        4  (0.90799234 , 0.98429713)
 
 StdErr = sqrt[1/r * MS(R)]
 DF = df[MS(R)] = r-1
 95% CI: AUC +- t(.025;df) * StdErr
 Note: this is the conventional CI, treating the reader AUCs as a random sample.
 
 
 
 #=> Reference resources are missing. Default references provided. <=#
 


                               REFERENCES

 1.   Dorfman, D.D., Berbaum, K.S., & Metz, C.E. (1992). Receiver operating
 characteristic rating analysis: Generalization to the population of 
 readers and patients with the jackknife method. Investigative Radiology,
 27, 723-731.

 2.    Obuchowski, N.A., & Rockette, H.E. (1995). Hypothesis testing of diagnostic
 accuracy for multiple readers and multiple tests: An ANOVA approach with dependent
 observations. Communications in Statistics-Simulation and Computation, 24, 285-308.

 3.   Hillis, S.L., Obuchowski, N.A., Schartz, K.M., & Berbaum, K.S.
 (2005). A comparison of the Dorfman-Berbaum-Metz and Obuchowski-Rockette
 methods for receiver operating characteristic (ROC) data. 
 Statistics in Medicine, 24, 1579-1607  DOI:10.1002/sim.2024.

 4.   Hillis, S.L. (2007). A comparison of denominator degrees of freedom for
 multiple observer ROC analysis.  Statistics in Medicine, 26:596-619  DOI:10.1002/sim.2532.

 6.   Hillis, S.L., Berbaum, K.S., & Metz, C.E. (2008). Recent developments in the
 Dorfman-Berbaum-Metz procedure for multireader ROC study analysis. Academic Radiology, 15, 
 647-661. DOI:10.1016/j.acra.2007.12.015
