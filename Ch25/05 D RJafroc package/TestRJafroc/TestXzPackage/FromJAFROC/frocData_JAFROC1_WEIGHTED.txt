JAFROC SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

================================================================================

JAFROC Version 4.2.1
Last Compilation Dates
	JAFROC_CORE.DLL : Aug 24 2014 15:46:33
	JAFROC.EXE      : Aug 24 2014 15:46:56

	Today's date: 4/13/2015 14:00:12


FOM selected         :     ANALYSIS_METHOD_JAFROC1_WEIGHTED
Input  Data Filename :     C:\Users\Xuetong Zhai\Documents\frocData.xlsx
Output Data Filename :     C:\Users\Xuetong Zhai\Documents\frocData_JAFROC1_WEIGHTED.txt

================================================================================
 Analysis method:         :  WEIGHTED JAFROC1 FOM DBM-MRMC SIGNIFICANCE TESTING
 Number of Readers        :  4
 Number of Treatments     :  2
 Number of Normal Cases   :  100
 Number of Abnormal Cases :  100
 fraction normal cases    :  0.500
 min    LESIONS_PER_IMAGE :  1
 max    LESIONS_PER_IMAGE :  3
 mean   LESIONS_PER_IMAGE :  1.42
 Total  LESIONS           :  142
 Inc. Loc. Frac.          :  0.065

================================================================================

For TRT = 4         , and RDR = 1         , max FPF =  0.400.
For TRT = 4         , and RDR = 3         , max FPF =  0.250.
For TRT = 4         , and RDR = 4         , max FPF =  0.500.
For TRT = 4         , and RDR = 5         , max FPF =  0.220.
For TRT = 5         , and RDR = 1         , max FPF =  0.420.
For TRT = 5         , and RDR = 3         , max FPF =  0.240.
For TRT = 5         , and RDR = 4         , max FPF =  0.480.
For TRT = 5         , and RDR = 5         , max FPF =  0.250.

================================================================================

 Avg. number of non-lesion localization marks per reader on normal images:  0.456.
 Avg. number of non-lesion localization marks per reader on abnormal images:  0.253.
 Avg. number of lesion localization marks per reader :  0.920.

================================================================================

 ====================================================================
 *****                        Overview                          *****
 ====================================================================
 Three analyses are presented: 
 (1) Analysis 1 treats both readers and cases as random samples
     --results apply to the reader and case populations;
 (2) Analysis 2 treats only cases as a random sample
     --results apply to the population of cases but only for the
     readers used in this study; and
 (3) Analysis 3 treats only readers as a random sample
     --results apply to the population of readers but only for the
     cases used in this study.
 
 For all three analyses, the null hypothesis of equal treatments is
 tested in part (a), treatment difference 95% confidence intervals
 are given in part (b), and treatment 95% confidence intervals are
 given in part (c).  Parts (a) and (b) are based on the treatment x
 reader x case ANOVA while part (c) is based on the reader x case
 ANOVA for the specified treatment; these ANOVA tables are displayed
 before the analyses.  Different error terms are used as indicated
 for parts (a), (b), and (c) according to whether readers and cases
 are treated as fixed or random factors.  Note that the treatment
 confidence intervals in part (c) are based only on the data for the
 specified treatment, rather than the pooled data.  Treatment
 difference 95% confidence intervals for each reader are presented
 in part (d) of Analysis 2; each interval is based on the treatment
 x case ANOVA table (not included) for the specified reader.
 ===========================================================================
 *****                            Estimates                            *****
 ===========================================================================

                        TREATMENT
              -----------------------
  READER      4            5         
----------    ----------   ----------
1             0.83411074   0.78504825
3             0.75265449   0.69459426
4             0.71517849   0.65984750
5             0.83000624   0.77656001
 
 
 TREATMENT MEANS (averaged across readers)
----------    -----------------------------
4             0.78298748
5             0.72901255
 
 

 TREATMENT MEAN DIFFERENCES
----------   ----------    -----------
4          - 5             0.05397493
 
 
 
 
 ===========================================================================
 *****                          ANOVA Tables                           *****
 ===========================================================================
 
 TREATMENT X READER X CASE ANOVA
 
Source            SS               DF             MS        
------   --------------------    ------   ------------------
     T             1.16531983         1           1.16531983
     R             4.33804307         3           1.44601436
     C           114.80140917       199           0.57689150
    TR             0.00429404         3           0.00143135
    TC            18.29997037       199           0.09195965
    RC            70.76452158       597           0.11853354
   TRC            36.70018944       597           0.06147435
 TOTAL           246.07374751      1599
 
 
 
 READER X CASE ANOVAs for each treatment
 
 
                        Mean Squares
 Source     df   4            5         
 ------    ---   ----------   ----------   
      R      3   0.68950013   0.75794558
      C    199   0.30444195   0.36440920
     RC    597   0.08485068   0.09515721 
 
 ===========================================================================
 *****                  Variance components estimates                  *****
 ===========================================================================
 
 DBM Variance Component Estimates
 (for sample size estimation for future studies)
 Note: These are unbiased ANOVA estimates which can be negative
 
     DBM Component            Estimate    
 -----------------------  ----------------
 Var(R)                         0.00346881
 Var(C)                         0.05348408
 Var(T*R)                      -0.00030022
 Var(T*C)                       0.00762132
 Var(R*C)                       0.02852959
 Var(T*R*C) + Var(Error)        0.06147435
 
 Obuchowski-Rockette variance component and covariance estimates
 (for sample size estimation for future studies)
 Note: These are unbiased ANOVA estimates which can be negative
 
     OR Component             Estimate    
 -----------------------  ----------------
 Reader                         0.00346881
 Treatment*Reader              -0.00030022
 COV1                           0.00041007
 COV2                           0.00030553
 COV3                           0.00026742
 ERROR                          0.00075555
 
 
 ===========================================================================
 *****           Analysis 1: Random Readers and Random Cases           *****
 ===========================================================================
 (Results apply to the population of readers and cases)


    a) Test for H0: Treatments have the same ANALYSIS_METHOD_JAFROC1_WEIGHTED
 
 Source        DF    Mean Square      F value  Pr > F 
 ----------  ------  ---------------  -------  -------
 Treatment        1       1.16531983    36.51  <0.0001
 Error       1491.64       0.03191664
 Error term: MS(TR) + max[MS(TC)-MS(TRC),0]
 
 Conclusion: The treatment ANALYSIS_METHOD_JAFROC1_WEIGHTEDs are not equal,
             F(1,1491.64) = 36.51, p = 0.0000.


    b) 95% confidence intervals for treatment differences
 
       Treatment         Estimate   StdErr      DF      t     Pr > t          95% CI      
----------   ----------  --------  --------  -------  ------  -------  -------------------
4          - 5            0.05397   0.00893  1491.64    6.04   0.0000   0.03645 ,  0.07150
 
 H0: the two treatments are equal.
 Error term: MS(TR) + max[MS(TC)-MS(TRC),0]
 

    c) 95% treatment confidence intervals based on reader x case ANOVAs
       for each treatment (each analysis is based only on data for the
       specified treatment
 
  Treatment     Area      Std Error     DF     95% Confidence Interval 
  ----------  ----------  ----------  -------  -------------------------
  4           0.78298750  0.03371000     5.22  (0.69739752 , 0.86857748)
  5           0.72901251  0.03583290     5.51  (0.63940741 , 0.81861761)
 Error term: MS(R) + max[MS(C)-MS(RC),0]
 
 
 
 ===========================================================================
 *****           Analysis 2: Fixed Readers and Random Cases            *****
 ===========================================================================
 
 (Results apply to the population of cases but only for the readers
 used in this study)
 
    a) Test for H0: Treatments have the same ANALYSIS_METHOD_JAFROC1_WEIGHTED
 
 Source        DF    Mean Square      F value  Pr > F 
 ----------  ------  ---------------  -------  -------
 Treatment        1       1.16531983    12.67   0.0005
 Error          199       0.09195965
 Error term: MS(TC)
 
 Conclusion: The treatment ANALYSIS_METHOD_JAFROC1_WEIGHTEDs are not equal,
             F(1,199.00) = 12.67, p = 0.0005.


    b) 95% confidence intervals for treatment differences
 
       Treatment         Estimate   StdErr      DF      t     Pr > t          95% CI      
----------   ----------  --------  --------  -------  ------  -------  -------------------
4          - 5            0.05397   0.01516      199    3.56   0.0005   0.02408 ,  0.08387
 
 H0: the two treatments are equal.
 Error term: MS(TC)
 

    c) 95% treatment confidence intervals based on reader x case ANOVAs
       for each treatment (each analysis is based only on data for the
       specified treatment
 
  Treatment     Area      Std Error     DF     95% Confidence Interval 
  ----------  ----------  ----------  -------  -------------------------
  4           0.78298750  0.01950775      199  (0.74451906 , 0.82145594)
  5           0.72901251  0.02134272      199  (0.68692560 , 0.77109942)
 Error term: MS(C)
 
 
 
 TREATMENT X CASE ANOVAs for each reader
 

                        Sum of Squares
 Source     df   1             3             4             5             
 ------    ---   -----------   -----------   -----------   -----------   
      T      1     0.2407127     0.3370994     0.3061516     0.2856501   
      C    199    39.5319466    47.3506375    58.3881717    40.2951749   
     TC    199    11.7077135    14.5945285    18.9724677     9.7254500   
 

                        Mean Squares
 Source     df   1             3             4             5             
 ------    ---   -----------   -----------   -----------   -----------   
      T      1     0.2407127     0.3370994     0.3061516     0.2856501   
      C    199     0.1986530     0.2379429     0.2934079     0.2024883   
     TC    199     0.0588327     0.0733393     0.0953390     0.0488716   
 
 


    d) Treatment-by-case ANOVA CIs for each reader 
       (each analysis is based only on data for the specified reader)
 
  Reader         Treatment        Estimate  StdErr       DF      t     Pr > t          95% CI      
---------- ---------- ----------  --------  --------  -------  ------  -------  -------------------
1          4         -5            0.04906   0.02426      199    2.02   0.0444   0.00123 ,  0.09689
3          4         -5            0.05806   0.02708      199    2.14   0.0333   0.00466 ,  0.11146
4          4         -5            0.05533   0.03088      199    1.79   0.0747  -0.00556 ,  0.11622
5          4         -5            0.05345   0.02211      199    2.42   0.0165   0.00985 ,  0.09704
 
 
 ===========================================================================
 *****           Analysis 3: Random Readers and Fixed Cases            *****
 ===========================================================================
 (Results apply to the population of readers but only for the cases used in this study)


    a) Test for H0: Treatments have the same ANALYSIS_METHOD_JAFROC1_WEIGHTED
 
 Source        DF    Mean Square      F value  Pr > F 
 ----------  ------  ---------------  -------  -------
 Treatment        1       1.16531983   814.14  <0.0001
 Error         3.00       0.00143135
 Error term: MS(TR)
 
 Conclusion: The treatment ANALYSIS_METHOD_JAFROC1_WEIGHTEDs are not equal,
             F(1,3.00) = 814.14, p = 0.0001.


    b) 95% confidence intervals for treatment differences
 
       Treatment         Estimate   StdErr      DF      t     Pr > t          95% CI      
----------   ----------  --------  --------  -------  ------  -------  -------------------
4          - 5            0.05397   0.00189     3.00   28.53   0.0001   0.04795 ,  0.06000
 
 H0: the two treatments are equal.
 
 

    c) Reader-by-case ANOVAs for each treatment (each analysis is based
       only on data for the specified treatment)
 

                        Mean Squares
 Source     df   4            5            
 ------    ---   ----------   ----------   
      R      3   0.68950013   0.75794558   
      C    199   0.30444195   0.36440920   
     RC    597   0.08485068   0.09515721   


 Estimates and 95% Confidence Intervals

  Treatment     Area      Std Error     DF     95% Confidence Interval 
  ----------  ----------  ----------  -------  -------------------------
  4           0.78298750  0.02935771        3  (0.68955816 , 0.87641684)
  5           0.72901251  0.03078038        3  (0.63105559 , 0.82696943)
 



                               REFERENCES

      Dorfman, D.D., Berbaum, K.S., & Metz, C.E. (1992). Receiver operating
 characteristic rating analysis: Generalization to the population of 
 readers and patients with the jackknife method. Investigative Radiology,
 27, 723-731.

      Dorfman, D.D., Berbaum, K.S., Lenth, R.V., Chen, Y.F., & Donaghy, B.A. (1998). 
 Monte Carlo validation of a multireader method for receiver operating characteristic 
 discrete rating data: Factorial experimental design. 
 Academic Radiology, 5, 591-602.

      Hillis, S.L., & Berbaum, K.S. (2004). Power estimation for the
 Dorfman-Berbaum-Metz method. Academic Radiology, 11, 1260-1273.

      Hillis, S.L., Obuchowski, N.A., Schartz, K.M., & Berbaum, K.S.
 (2005). A comparison of the Dorfman-Berbaum-Metz and Obuchowski-Rockette
 methods for receiver operating characteristic (ROC) data. 
 Statistics in Medicine, 24, 1579-1607  DOI:10.1002/sim.2024.

      Hillis, S.L. (2005). Monte Carlo validation of the Dorfman-Berbaum-Metz
 method using normalized pseudovalues and less data-based model simplification
 Academic Radiology, 12:1534-1541  DOI:10.1016/j.acra.2005.07.012.

      Hillis, S.L. (2007). A comparison of denominator degrees of freedom for
 multiple observer ROC analysis.  Statistics in Medicine, 26:596-619  DOI:10.1002/sim.2532.

      Hillis, S.L., Berbaum, K.S., & Metz, C.E. (2008). Recent developments in the
 Dorfman-Berbaum-Metz procedure for multireader ROC study analysis. Academic Radiology, in press.

================================================================================
