JAFROC SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

================================================================================

JAFROC Version 4.2.1
Last Compilation Dates
	JAFROC_CORE.DLL : Aug 24 2014 15:46:33
	JAFROC.EXE      : Aug 24 2014 15:46:56

	Today's date: 4/13/2015 14:00:04


FOM selected         :     ANALYSIS_METHOD_JAFROC1
Input  Data Filename :     C:\Users\Xuetong Zhai\Documents\frocData.xlsx
Output Data Filename :     C:\Users\Xuetong Zhai\Documents\frocData_JAFROC1.txt

================================================================================
 Analysis method:         :  JAFROC1 FOM DBM-MRMC SIGNIFICANCE TESTING
 Number of Readers        :  4
 Number of Treatments     :  2
 Number of Normal Cases   :  100
 Number of Abnormal Cases :  100
 fraction normal cases    :  0.500
 min    LESIONS_PER_IMAGE :  1
 max    LESIONS_PER_IMAGE :  3
 mean   LESIONS_PER_IMAGE :  1.42
 Total  LESIONS           :  142
 Inc. Loc. Frac.          :  0.065

================================================================================

For TRT = 4         , and RDR = 1         , max FPF =  0.400.
For TRT = 4         , and RDR = 3         , max FPF =  0.250.
For TRT = 4         , and RDR = 4         , max FPF =  0.500.
For TRT = 4         , and RDR = 5         , max FPF =  0.220.
For TRT = 5         , and RDR = 1         , max FPF =  0.420.
For TRT = 5         , and RDR = 3         , max FPF =  0.240.
For TRT = 5         , and RDR = 4         , max FPF =  0.480.
For TRT = 5         , and RDR = 5         , max FPF =  0.250.

================================================================================

 Avg. number of non-lesion localization marks per reader on normal images:  0.456.
 Avg. number of non-lesion localization marks per reader on abnormal images:  0.253.
 Avg. number of lesion localization marks per reader :  0.920.

================================================================================

 ====================================================================
 *****                        Overview                          *****
 ====================================================================
 Three analyses are presented: 
 (1) Analysis 1 treats both readers and cases as random samples
     --results apply to the reader and case populations;
 (2) Analysis 2 treats only cases as a random sample
     --results apply to the population of cases but only for the
     readers used in this study; and
 (3) Analysis 3 treats only readers as a random sample
     --results apply to the population of readers but only for the
     cases used in this study.
 
 For all three analyses, the null hypothesis of equal treatments is
 tested in part (a), treatment difference 95% confidence intervals
 are given in part (b), and treatment 95% confidence intervals are
 given in part (c).  Parts (a) and (b) are based on the treatment x
 reader x case ANOVA while part (c) is based on the reader x case
 ANOVA for the specified treatment; these ANOVA tables are displayed
 before the analyses.  Different error terms are used as indicated
 for parts (a), (b), and (c) according to whether readers and cases
 are treated as fixed or random factors.  Note that the treatment
 confidence intervals in part (c) are based only on the data for the
 specified treatment, rather than the pooled data.  Treatment
 difference 95% confidence intervals for each reader are presented
 in part (d) of Analysis 2; each interval is based on the treatment
 x case ANOVA table (not included) for the specified reader.
 ===========================================================================
 *****                            Estimates                            *****
 ===========================================================================

                        TREATMENT
              -----------------------
  READER      4            5         
----------    ----------   ----------
1             0.80878520   0.75808096
3             0.73468310   0.68257040
4             0.73434860   0.66436619
5             0.81556338   0.77427816
 
 
 TREATMENT MEANS (averaged across readers)
----------    -----------------------------
4             0.77334505
5             0.71982396
 
 

 TREATMENT MEAN DIFFERENCES
----------   ----------    -----------
4          - 5             0.05352110
 
 
 
 
 ===========================================================================
 *****                          ANOVA Tables                           *****
 ===========================================================================
 
 TREATMENT X READER X CASE ANOVA
 
Source            SS               DF             MS        
------   --------------------    ------   ------------------
     T             1.14580514         1           1.14580514
     R             2.94616078         3           0.98205359
     C            96.08482716       199           0.48283833
    TR             0.04306107         3           0.01435369
    TC            20.28072764       199           0.10191320
    RC            63.72700826       597           0.10674541
   TRC            34.05564913       597           0.05704464
 TOTAL           218.28323918      1599
 
 
 
 READER X CASE ANOVAs for each treatment
 
 
                        Mean Squares
 Source     df   4            5         
 ------    ---   ----------   ----------   
      R      3   0.40359059   0.59281669
      C    199   0.26669561   0.31805593
     RC    597   0.07670598   0.08708407 
 
 ===========================================================================
 *****                  Variance components estimates                  *****
 ===========================================================================
 
 DBM Variance Component Estimates
 (for sample size estimation for future studies)
 Note: These are unbiased ANOVA estimates which can be negative
 
     DBM Component            Estimate    
 -----------------------  ----------------
 Var(R)                         0.00229500
 Var(C)                         0.04140304
 Var(T*R)                      -0.00021345
 Var(T*C)                       0.01121714
 Var(R*C)                       0.02485038
 Var(T*R*C) + Var(Error)        0.05704464
 
 Obuchowski-Rockette variance component and covariance estimates
 (for sample size estimation for future studies)
 Note: These are unbiased ANOVA estimates which can be negative
 
     OR Component             Estimate    
 -----------------------  ----------------
 Reader                         0.00229500
 Treatment*Reader              -0.00021345
 COV1                           0.00033127
 COV2                           0.00026310
 COV3                           0.00020702
 ERROR                          0.00067258
 
 
 ===========================================================================
 *****           Analysis 1: Random Readers and Random Cases           *****
 ===========================================================================
 (Results apply to the population of readers and cases)


    a) Test for H0: Treatments have the same ANALYSIS_METHOD_JAFROC1
 
 Source        DF    Mean Square      F value  Pr > F 
 ----------  ------  ---------------  -------  -------
 Treatment        1       1.14580514    19.35  <0.0001
 Error        51.07       0.05922226
 Error term: MS(TR) + max[MS(TC)-MS(TRC),0]
 
 Conclusion: The treatment ANALYSIS_METHOD_JAFROC1s are not equal,
             F(1,51.07) = 19.35, p = 0.0001.


    b) 95% confidence intervals for treatment differences
 
       Treatment         Estimate   StdErr      DF      t     Pr > t          95% CI      
----------   ----------  --------  --------  -------  ------  -------  -------------------
4          - 5            0.05352   0.01217    51.07    4.40   0.0001   0.02909 ,  0.07795
 
 H0: the two treatments are equal.
 Error term: MS(TR) + max[MS(TC)-MS(TRC),0]
 

    c) 95% treatment confidence intervals based on reader x case ANOVAs
       for each treatment (each analysis is based only on data for the
       specified treatment
 
  Treatment     Area      Std Error     DF     95% Confidence Interval 
  ----------  ----------  ----------  -------  -------------------------
  4           0.77334507  0.02723922     6.49  (0.70789257 , 0.83879756)
  5           0.71982392  0.03208949     5.79  (0.64061898 , 0.79902886)
 Error term: MS(R) + max[MS(C)-MS(RC),0]
 
 
 
 ===========================================================================
 *****           Analysis 2: Fixed Readers and Random Cases            *****
 ===========================================================================
 
 (Results apply to the population of cases but only for the readers
 used in this study)
 
    a) Test for H0: Treatments have the same ANALYSIS_METHOD_JAFROC1
 
 Source        DF    Mean Square      F value  Pr > F 
 ----------  ------  ---------------  -------  -------
 Treatment        1       1.14580514    11.24   0.0010
 Error          199       0.10191320
 Error term: MS(TC)
 
 Conclusion: The treatment ANALYSIS_METHOD_JAFROC1s are not equal,
             F(1,199.00) = 11.24, p = 0.0010.


    b) 95% confidence intervals for treatment differences
 
       Treatment         Estimate   StdErr      DF      t     Pr > t          95% CI      
----------   ----------  --------  --------  -------  ------  -------  -------------------
4          - 5            0.05352   0.01596      199    3.35   0.0010   0.02204 ,  0.08500
 
 H0: the two treatments are equal.
 Error term: MS(TC)
 

    c) 95% treatment confidence intervals based on reader x case ANOVAs
       for each treatment (each analysis is based only on data for the
       specified treatment
 
  Treatment     Area      Std Error     DF     95% Confidence Interval 
  ----------  ----------  ----------  -------  -------------------------
  4           0.77334507  0.01825841      199  (0.73734028 , 0.80934986)
  5           0.71982392  0.01993916      199  (0.68050478 , 0.75914307)
 Error term: MS(C)
 
 
 
 TREATMENT X CASE ANOVAs for each reader
 

                        Sum of Squares
 Source     df   1             3             4             5             
 ------    ---   -----------   -----------   -----------   -----------   
      T      1     0.2570919     0.2715733     0.4897541     0.1704469   
      C    199    36.5582253    40.3946084    43.9665104    38.8924913   
     TC    199     9.9240176    14.8037142    18.5992610    11.0093840   
 

                        Mean Squares
 Source     df   1             3             4             5             
 ------    ---   -----------   -----------   -----------   -----------   
      T      1     0.2570919     0.2715733     0.4897541     0.1704469   
      C    199     0.1837097     0.2029880     0.2209372     0.1954397   
     TC    199     0.0498694     0.0743905     0.0934636     0.0553235   
 
 


    d) Treatment-by-case ANOVA CIs for each reader 
       (each analysis is based only on data for the specified reader)
 
  Reader         Treatment        Estimate  StdErr       DF      t     Pr > t          95% CI      
---------- ---------- ----------  --------  --------  -------  ------  -------  -------------------
1          4         -5            0.05070   0.02233      199    2.27   0.0242   0.00667 ,  0.09474
3          4         -5            0.05211   0.02727      199    1.91   0.0575  -0.00167 ,  0.10590
4          4         -5            0.06998   0.03057      199    2.29   0.0231   0.00970 ,  0.13027
5          4         -5            0.04129   0.02352      199    1.76   0.0808  -0.00510 ,  0.08767
 
 
 ===========================================================================
 *****           Analysis 3: Random Readers and Fixed Cases            *****
 ===========================================================================
 (Results apply to the population of readers but only for the cases used in this study)


    a) Test for H0: Treatments have the same ANALYSIS_METHOD_JAFROC1
 
 Source        DF    Mean Square      F value  Pr > F 
 ----------  ------  ---------------  -------  -------
 Treatment        1       1.14580514    79.83   0.0030
 Error         3.00       0.01435369
 Error term: MS(TR)
 
 Conclusion: The treatment ANALYSIS_METHOD_JAFROC1s are not equal,
             F(1,3.00) = 79.83, p = 0.0030.


    b) 95% confidence intervals for treatment differences
 
       Treatment         Estimate   StdErr      DF      t     Pr > t          95% CI      
----------   ----------  --------  --------  -------  ------  -------  -------------------
4          - 5            0.05352   0.00599     3.00    8.93   0.0030   0.03446 ,  0.07259
 
 H0: the two treatments are equal.
 
 

    c) Reader-by-case ANOVAs for each treatment (each analysis is based
       only on data for the specified treatment)
 

                        Mean Squares
 Source     df   4            5            
 ------    ---   ----------   ----------   
      R      3   0.40359059   0.59281669   
      C    199   0.26669561   0.31805593   
     RC    597   0.07670598   0.08708407   


 Estimates and 95% Confidence Intervals

  Treatment     Area      Std Error     DF     95% Confidence Interval 
  ----------  ----------  ----------  -------  -------------------------
  4           0.77334507  0.02246082        3  (0.70186473 , 0.84482541)
  5           0.71982392  0.02722170        3  (0.63319233 , 0.80645552)
 



                               REFERENCES

      Dorfman, D.D., Berbaum, K.S., & Metz, C.E. (1992). Receiver operating
 characteristic rating analysis: Generalization to the population of 
 readers and patients with the jackknife method. Investigative Radiology,
 27, 723-731.

      Dorfman, D.D., Berbaum, K.S., Lenth, R.V., Chen, Y.F., & Donaghy, B.A. (1998). 
 Monte Carlo validation of a multireader method for receiver operating characteristic 
 discrete rating data: Factorial experimental design. 
 Academic Radiology, 5, 591-602.

      Hillis, S.L., & Berbaum, K.S. (2004). Power estimation for the
 Dorfman-Berbaum-Metz method. Academic Radiology, 11, 1260-1273.

      Hillis, S.L., Obuchowski, N.A., Schartz, K.M., & Berbaum, K.S.
 (2005). A comparison of the Dorfman-Berbaum-Metz and Obuchowski-Rockette
 methods for receiver operating characteristic (ROC) data. 
 Statistics in Medicine, 24, 1579-1607  DOI:10.1002/sim.2024.

      Hillis, S.L. (2005). Monte Carlo validation of the Dorfman-Berbaum-Metz
 method using normalized pseudovalues and less data-based model simplification
 Academic Radiology, 12:1534-1541  DOI:10.1016/j.acra.2005.07.012.

      Hillis, S.L. (2007). A comparison of denominator degrees of freedom for
 multiple observer ROC analysis.  Statistics in Medicine, 26:596-619  DOI:10.1002/sim.2532.

      Hillis, S.L., Berbaum, K.S., & Metz, C.E. (2008). Recent developments in the
 Dorfman-Berbaum-Metz procedure for multireader ROC study analysis. Academic Radiology, in press.

================================================================================
